FROM nginx:alpine

COPY index.html /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

# docker build -t devopster:3 .
# docker run -p 80:80 devopster:3